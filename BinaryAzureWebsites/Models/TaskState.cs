﻿namespace BinaryAzureWebsites.Models
{
    public enum TaskState
    {
        IsCreate,

        IsProgress,

        IsTesting,

        IsDone
    }
}

﻿using System;

namespace BinaryAzureWebsites.Models
{
    public struct UserInfo
    {
        public User User { get; set; }
        public Project Project { get; set; }
        public int CountTasksInProject { get; set; }
        public int CountTasksInWork { get; set; }
        public Tasks Tasks { get; set; }
    }
}

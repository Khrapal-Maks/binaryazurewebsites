﻿using System.Collections.Generic;

namespace BinaryAzureWebsites.Models
{
    public class DataModel
    {
        public Project Project { get; set; }

        public List<Tasks> Tasks { get; set; }

        public User Performer { get; set; }

        public User Author { get; set; }

        public Team Team { get; set; }
    }
}

﻿using BinaryAzureWebsites.Services;
using System;
using System.Text;

namespace BinaryAzureWebsites
{
    public class MenuTasks
    {
        private OperationService _operationService;

        public MenuTasks()
        {
            _operationService = new();
            GetMenu();
        }

        private void GetMenu()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n\n\n\n\n\n\t\t\t\t\t\tSelect Task\n");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("\t\t\t    Name operation:                       Select operation\n" +
                              "                                                                  and enter number. ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(
                "\t\t\t\tTask 1                                   1\n" +
                "\t\t\t\tTask 2                                   2\n" +
                "\t\t\t\tTask 3                                   3\n" +
                "\t\t\t\tTask 4                                   4\n" +
                "\t\t\t\tTask 5                                   5\n" +
                "\t\t\t\tTask 6                                   6\n" +
                "\t\t\t\tTask 7                                   7\n");

            ValidationMenu();
        }

        private void ValidationMenu()
        {
            int selection;

            while (!int.TryParse(Console.ReadLine(), out selection))
            {
                Error();
            }
            GetOperation(selection);
        }

        private void GetOperation(int selection)
        {
            switch (selection)
            {
                case 1:
                    _operationService.GetTaskOne();
                    ConsoleClearForMenu();
                    break;
                case 2:
                    _operationService.GetTaskSecond();
                    ConsoleClearForMenu();
                    break;
                case 3:
                    _operationService.GetTaskThird();
                    ConsoleClearForMenu();
                    break;
                case 4:
                    _operationService.GetTaskFour();
                    ConsoleClearForMenu();
                    break;
                case 5:
                    _operationService.GetTaskFive();
                    ConsoleClearForMenu();
                    break;
                case 6:
                    _operationService.GetTaskSix();
                    ConsoleClearForMenu();
                    break;
                case 7:
                    _operationService.GetTaskSeven();
                    ConsoleClearForMenu();
                    break;
                default:
                    Console.Clear();
                    Error();
                    break;
            }
        }

        private void ConsoleClearForMenu()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\n\n\nPress 'enter' to return to main menu...");
            Console.ReadKey();
            Console.Clear();
            GetMenu();
        }

        private void Error()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Ошибка ввода, проверьте правильность ввода!");
            ConsoleClearForMenu();
        }
    }
}

﻿using BinaryAzureWebsites.Models;
using BinaryAzureWebsites.Services;
using System.Collections.Generic;
using System.Linq;

namespace BinaryAzureWebsites.Data
{
    public class DataProject
    {
        private readonly WebService _webService;
        private List<Project> _projects;
        private List<Tasks> _tasks;
        private List<User> _users;
        private List<Team> _teams;

        public List<DataModel> DataModel { get; private set; }
        public DataProject()
        {
            _webService = new();
            _projects = _webService.GetAllProjects().Result;
            _tasks = _webService.GetAllTasks().Result;
            _users = _webService.GetAllUsers().Result;
            _teams = _webService.GetAllTeams().Result;
            GetData();
        }
        private void GetData()
        {
            DataModel = _projects.GroupJoin(_tasks,
            project => project.Id,
            task => task.ProjectId,
            (project, task) => new { Project = project, Tasks = task })
            .Zip(_users,
            (task, performer) => new { Task = task, Performer = performer})
            .Join(_users,
            project => project.Task.Project.AuthorId,
            user => user.Id,
            (project, author) => new { Project = project, Author = author })
            .Join(_teams,
            project => project.Project.Task.Project.TeamId,
            team => team.Id,
            (project, team) => new DataModel
            { 
                Project = project.Project.Task.Project,
                Tasks = project.Project.Task.Tasks.ToList(),
                Performer = project.Project.Performer,
                Author = project.Author,
                Team = team
            }).ToList();
        }        
    }
}

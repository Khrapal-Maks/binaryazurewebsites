﻿using BinaryAzureWebsites.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace BinaryAzureWebsites.Services
{
    public class WebService
    {
        private const string API_PATH = "https://bsa21.azurewebsites.net/api/";

        private HttpClient _client = new();

        public WebService()
        {
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<Project>> GetAllProjects()
        {
            var response = await _client.GetAsync($"{API_PATH}/Projects");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Project>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Project> GetProject(int id)
        {
            var response = await _client.GetAsync($"{API_PATH}/Projects/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<Project>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<Tasks>> GetAllTasks()
        {
            var response = await _client.GetAsync($"{API_PATH}/Tasks");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Tasks>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Tasks> GetTask(int id)
        {
            var response = await _client.GetAsync($"{API_PATH}/Tasks/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<Tasks>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<Team>> GetAllTeams()
        {
            var response = await _client.GetAsync($"{API_PATH}/Teams");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Team>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Team> GetTeam(int id)
        {
            var response = await _client.GetAsync($"{API_PATH}/Teams/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<Team>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<User>> GetAllUsers()
        {
            var response = await _client.GetAsync($"{API_PATH}/Users");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<User>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<User> GetlUser(int id)
        {
            var response = await _client.GetAsync($"{API_PATH}/Users/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<User>(await response.Content.ReadAsStringAsync());
        }
    }
}

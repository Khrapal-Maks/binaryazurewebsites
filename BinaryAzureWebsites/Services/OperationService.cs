﻿using BinaryAzureWebsites.Models;
using System;
using System.Collections.Generic;

namespace BinaryAzureWebsites.Services
{
    public class OperationService
    {
        private DataQueriesService _dataQueriesService;

        public OperationService()
        {
            _dataQueriesService = new();
        }

        #region Task 1
        //Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь, где ключом будет проект, а значением кол-во тасков).
        //Example author id 77
        public void GetTaskOne()
        {
            int authorId = 77;
            var projectsTasks = _dataQueriesService.GetAllTasksInProjectsByAuthor(authorId);

            Console.Clear();
            Console.WriteLine("Task 1: Example author id 77");
            Console.WriteLine("{0,-10} | {1,-55} | {2,-40}", "Project id", "Name project", "Count all tasks in project");
            foreach (var item in projectsTasks)
            {
                Console.WriteLine("{0,-10} | {1,-55} | {2,-40}", item.Key.Id, item.Key.Name, item.Value);
            }
        }
        #endregion

        #region Task 2
        //Получить список тасков, назначенных на конкретного пользователя (по id), где name таска < 45 символов (коллекция из тасков).
        //Example performer id 73

        public void GetTaskSecond()
        {
            int performerId = 73;
            var tasks = _dataQueriesService.GetAllTasksOnPerformer(performerId);

            Console.Clear();
            Console.WriteLine("Task 2: Example performer id 73");
            foreach (var task in tasks)
            {
                Console.WriteLine($"Task id: {task.Id}\ttask name: {task.Name}\tperformer id: {task.PerformerId} etc.");
            }
        }
        #endregion

        #region Task 3
        //Получить список (id, name) из коллекции тасков, которые выполнены (finished) в текущем (2021) году для конкретного пользователя (по id).
        //Example performer id 20 = true
        public void GetTaskThird()
        {
            int performerId = 20;
            var tasks = _dataQueriesService.GetAllTasksThatFinished(performerId);

            Console.Clear();
            Console.WriteLine("Task 3: Example performer id 20 = true");
            Console.WriteLine("{0,-12} | {1,-8}", "Task id", "Task name");
            foreach (var item in tasks)
            {
                Console.WriteLine("{0,-12} | {1,-8} ", item.Item1, item.Item2);
            }
        }
        #endregion

        #region Task 4
        //Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет, отсортированных по дате регистрации пользователя по убыванию, а также
        //сгруппированных по командам. P.S - в этом запросе допускается проверить только год рождения пользователя, без привязки к месяцу / дню / времени рождения.
        public void GetTaskFour()
        {
            var usersTeams = _dataQueriesService.GetAllUsersOldestThanTenYears();

            Console.Clear();
            Console.WriteLine("Task 4");
            foreach (var item in usersTeams)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"\nTeam id: {item.Item1}\tTeam name: {item.Item2}");
                foreach (var user in item.Item3)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"Date registration: {user.RegisteredAt}\tUser id: {user.Id}\tUser name: {user.FirstName + " " + user.LastName}");
                }
            }
        }
        #endregion

        #region Task 5
        //Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).
        public void GetTaskFive()
        {
            var allUsersWithTheirTasks = _dataQueriesService.SortAllUsersFirstNameAndSortTaskOnName();

            Console.Clear();
            Console.WriteLine("Task 5");
            foreach (var user in allUsersWithTheirTasks)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"User name: {user.Item1}");
                Console.ForegroundColor = ConsoleColor.White;
                foreach (var task in user.Item2)
                {
                    Console.WriteLine($"Task Id: {task.Id}\ttask name: {task.Name}");
                }
                Console.WriteLine();
            }
        }
        #endregion

        #region Task 6
        //Получить следующую структуру (передать Id пользователя в параметры):
        //User
        //Последний проект пользователя(по дате создания)
        //Общее кол-во тасков под последним проектом
        //Общее кол-во незавершенных или отмененных тасков для пользователя
        //Самый долгий таск пользователя по дате(раньше всего создан -позже всего закончен)
        //P.S. - в данном случае, статус таска не имеет значения, фильтруем только по дате.
        //Example user id 77
        public void GetTaskSix()
        {
            int id = 73;
            List<UserInfo> userInfos = new()
            {
                _dataQueriesService.GetStructUserById(id)
            };

            Console.Clear();
            Console.WriteLine("Tasl 6: example user id 73");
            foreach (var user in userInfos)
            {
                Console.WriteLine($"User id: {user.User.Id}\tUser name: {user.User.FirstName + " " + user.User.LastName} etc.");
                Console.WriteLine($"Project id: {user.Project.Id}\tProject name: {user.Project.Name} etc.");
                Console.WriteLine($"Count all tasks in project: {user.CountTasksInProject}");
                Console.WriteLine($"Count all tasks user in work: {user.CountTasksInWork}");
                Console.WriteLine($"The longest task in work: task id: {user.CountTasksInProject}\tperformer id: {user.Tasks.PerformerId}\tproject id: {user.Tasks.ProjectId} etc.");
            }
        }
        #endregion

        #region Task 7
        //Получить следующую структуру:
        //Проект
        //Самый длинный таск проекта(по описанию)
        //Самый короткий таск проекта(по имени)
        //Общее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол - во тасков < 3
        public void GetTaskSeven()
        {
           var projectInfo = _dataQueriesService.GetStructAllProjects();

            Console.Clear();
            Console.WriteLine("Tasl 7");
            foreach (var project in projectInfo)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Project id: {project.Project.Id}\tProject name: {project.Project.Name}");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"The longest task description in project: id {project.TaskLongDescription.Id}\ttext: {project.TaskLongDescription.Name}");
                Console.WriteLine($"The shortest task name in project:       id {project.TasksShortName.Id}\ttext: {project.TasksShortName.Name}");
                if(project.CountUsersInTeamProject == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                Console.WriteLine($"Count users in team: {project.CountUsersInTeamProject}\n");
            }
        }
        #endregion
    }
}

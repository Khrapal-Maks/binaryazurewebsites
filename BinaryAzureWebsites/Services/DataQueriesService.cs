﻿using BinaryAzureWebsites.Data;
using BinaryAzureWebsites.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BinaryAzureWebsites.Services
{
    public class DataQueriesService
    {
        private DataProject _dataProject;

        public DataQueriesService()
        {
            _dataProject = new();
        }

        public Dictionary<Project, int> GetAllTasksInProjectsByAuthor(int authorId)
        {
            return _dataProject.DataModel.Select(x => new { x.Project, x.Tasks }).Where(x => x.Project.AuthorId == authorId)
                .ToDictionary
                (
                    k => new Project
                    {
                        Id = k.Project.Id,
                        AuthorId = k.Project.AuthorId,
                        CreatedAt = k.Project.CreatedAt,
                        Deadline = k.Project.Deadline,
                        Description = k.Project.Description,
                        Name = k.Project.Name,
                        TeamId = k.Project.TeamId
                    },
                    v => v.Tasks.Count
                );
        }

        public List<Tasks> GetAllTasksOnPerformer(int performerId)
        {
            int lengthNameTask = 45;

            return _dataProject.DataModel.SelectMany(x => x.Tasks.Where(x => x.PerformerId == performerId & x.Name.Length < lengthNameTask)).ToList();
        }

        public List<Tuple<int, string>> GetAllTasksThatFinished(int performerId)
        {
            int year = 2021;

            return _dataProject.DataModel.SelectMany(x => x.Tasks.Where(x => x.PerformerId == performerId & x.FinishedAt.HasValue && x.FinishedAt.Value.Year == year)).Select(x => Tuple.Create(x.Id, x.Name)).ToList();
        }

        public List<Tuple<int, string, List<User>>> GetAllUsersOldestThanTenYears()
        {
            int ageUser = 10;

            return _dataProject.DataModel.Select(x => x.Team)
                .GroupJoin(_dataProject.DataModel.Select(x => x.Performer)
                .Where(x => x.BirthDay.Year + ageUser <= DateTime.Now.Year).OrderByDescending(x => x.RegisteredAt),
                team => team.Id,
                performer => performer.TeamId,
                (tm, pr) => new
                {
                    Team = tm,
                    Users = pr
                }).Select(x => Tuple.Create(x.Team, x.Users)).Distinct()
                .Select(x => Tuple.Create(x.Item1.Id, x.Item1.Name, x.Item2.ToList())).ToList();
        }
        public List<Tuple<string, List<Tasks>>> SortAllUsersFirstNameAndSortTaskOnName()
        {
            return _dataProject.DataModel.Select(x => x.Performer).OrderBy(x => x.FirstName)
                .Zip(_dataProject.DataModel.Select(x => x.Tasks.OrderByDescending(x => x.Name.Length)))
                .Select(x => Tuple.Create(x.First.FirstName, x.Second.ToList())).Where(x => x.Item2.Count != 0).ToList();
        }

        public UserInfo GetStructUserById(int userId)
        {
            return new UserInfo
            {
                User = _dataProject.DataModel.Find(x => x.Author.Id == userId).Author,

                Project = _dataProject.DataModel
                .FindAll(x => x.Project.AuthorId == userId).OrderByDescending(x => x.Project.CreatedAt).First().Project,

                CountTasksInProject = _dataProject.DataModel
                .FindAll(x => x.Project.AuthorId == userId).OrderByDescending(x => x.Project.CreatedAt).First().Tasks.Count,

                CountTasksInWork = _dataProject.DataModel
                .SelectMany(x => x.Tasks.Where(x => x.PerformerId == userId)).Count(),

                Tasks = _dataProject.DataModel
                .SelectMany(x => x.Tasks.Where(x => x.PerformerId == userId)).OrderBy(x => x.FinishedAt - x.CreatedAt).First()
            };
        }

        public List<ProjectInfo> GetStructAllProjects()
        {
            int descriptionLength = 20;
            int countTask = 3;          

            return _dataProject.DataModel.Select(x => x.Project)
                .GroupJoin(_dataProject.DataModel.SelectMany(x => x.Tasks),
                project => project.Id,
                task => task.ProjectId,
                (project, task) => new { Projects = project, Tasks = task })
                .Join(_dataProject.DataModel.Select(x => x.Team),
                project => project.Projects.TeamId,
                team => team.Id,
                (project, team) => new { Project = project, Team = team })
                .GroupJoin(_dataProject.DataModel.Select(x => x.Performer),
                project => project.Team.Id,
                user => user.TeamId,
                (project, user) => new ProjectInfo
                {
                    Project = project.Project.Projects,
                    TaskLongDescription = project.Project.Tasks.OrderBy(x => x.Description.Length).FirstOrDefault() ?? new Tasks(),
                    TasksShortName = project.Project.Tasks.OrderBy(x => x.Name.Length).FirstOrDefault() ?? new Tasks(),
                    CountUsersInTeamProject =
                    project.Project.Projects.Description.Length > descriptionLength == true | project.Project.Tasks.Count() < countTask == true
                    ? user.Count() : 0
                }).Distinct().ToList();
        }
    }
}
